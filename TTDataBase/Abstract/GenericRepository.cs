﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTDataBase.DataBase;

namespace TTDataBase.Abstract
{
    public abstract class GenericRepository<C,T>
        : IGenericRepository<T> where T : class where C : TaskerDataBase, new()
    {
        private C _entities = new C();
        public virtual void Create(T obj)
        {
            _entities.Set<T>().Add(obj);
            _entities.SaveChanges();
        }

        public virtual void Delete(T obj)
        {
            _entities.Entry(obj).State = EntityState.Deleted;
            _entities.SaveChanges();
        }

        public virtual void Delete(int? id)
        {
            T _obj = _entities.Set<T>().Find(id);
            if (_obj != null)
            {
                _entities.Entry(_obj).State = EntityState.Deleted;
                _entities.SaveChanges();
            }
        }

        public void Dispose()
        {
            _entities.Dispose();
        }

        public T FindById(int? id)
        {
            T _obj = _entities.Set<T>().Find(id);
            if (_obj != null)
            {
                return _obj;
            }
            return null;
        }

        public virtual IEnumerable<T> getAll()
        {
            List<T> _obj = _entities.Set<T>().ToList();
            return _obj;
        }

        public virtual void Update(T obj)
        {
            if (obj != null)
            {
                _entities.Entry(obj).State = EntityState.Modified;
                _entities.SaveChanges();
            }
        }
    }
}
