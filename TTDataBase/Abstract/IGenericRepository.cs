﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTDataBase.Abstract
{
    public interface IGenericRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> getAll();
        void Delete(int? id);
        void Delete(T obj);
        void Create(T obj);
        void Update(T obj);
        T FindById(int? id);
    }
}
