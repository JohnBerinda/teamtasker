﻿namespace TTDataBase.DataBase.Model
{
    public class PriorityModel
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }

    }
}