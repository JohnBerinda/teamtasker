﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TTDataBase.DataBase.Model
{
    public class TaskModel
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Title { get; set; }
        public DateTime finishDate { get; set; }
        [Required]
        public string Content { get; set; }
        public CheckBoxModel CheckBox { get; set; }
        public virtual ICollection<CommentModel> Comment { get; set; }
        public virtual ICollection<UserModel> User { get; set; }
        public PriorityTaskModel Priority { get; set; }

    }
}