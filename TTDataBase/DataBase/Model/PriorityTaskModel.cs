﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTDataBase.DataBase.Model
{
    public class PriorityTaskModel
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string title { get; set; }
        public virtual ICollection<PriorityModel> PriorityList { get; set; }
        public PriorityTaskModel()
        {
            PriorityList = new List<PriorityModel>();
        }

    }
}
