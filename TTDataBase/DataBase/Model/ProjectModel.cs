﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTDataBase.DataBase.Model
{
    public class ProjectModel
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Title { get; set; }
        public virtual ICollection<TaskTableModel> TaskList { get; set; }
        public virtual ICollection<UserModel> UserList { get; set; }
        public ProjectModel()
        {
            TaskList = new List<TaskTableModel>();
            UserList = new List<UserModel>();
        }
    }
}
