﻿using System.ComponentModel.DataAnnotations;

namespace TTDataBase.DataBase.Model
{
    public class CheckBoxElementModel
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Title { get; set; }
        public bool Status { get; set; }
        public CheckBoxElementModel()
        {
            Status = false;
        }
    }
}