﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTDataBase.DataBase.Model
{
    public class CheckBoxModel
    {
        [Key]
        public int? Id { get; set; }
        private string title = "Перечень заданий";
        public virtual ICollection<CheckBoxElementModel> CheckBoxElementList { get; set; }
        public CheckBoxModel()
        {
            CheckBoxElementList = new List<CheckBoxElementModel>();
        }
    }
}
