﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTDataBase.DataBase.Model
{
    public class TaskTableModel
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Title { get; set; }
        public virtual ICollection<TaskModel> TaskList { get; set; }
        public TaskTableModel()
        {
            TaskList = new List<TaskModel>();
        }
    }
}
