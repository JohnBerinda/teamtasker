﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTDataBase.DataBase.Model
{
    public class CommentModel
    {
        [Key]
        public int? Id { get; set; }
        [Required]
        public string Content { get; set; }
        public TaskModel task_id { get; set; }
        public UserModel User { get; set; }

    }
}
