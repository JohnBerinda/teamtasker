﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TeamTasker.Startup))]
namespace TeamTasker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
